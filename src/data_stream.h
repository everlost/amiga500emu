#pragma once
#include <vector>

template<typename T>
class DataStream {
private:
	std::vector<T> source_;
	size_t position_;
public:
	DataStream(const std::vector<T>& source):
		source_(source), position_(0) {}
	~DataStream() = default;

	T& peek() {
		return source_[position_++];
	}

	void unpeek() {
		--position_;
	}

	size_t getPosition() const {
		return position_;
	}
};