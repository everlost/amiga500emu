#include <iostream>
#include <vector>
#include <fstream>
#include <stdint.h>
#include "context.h"
#include "cpu.h"

bool ReadFile(const char* file, std::vector<uint8_t>& out) {
	std::ifstream f(file, std::ios::binary | std::ios::in);
	if (!f.good()) return false;
	f.seekg(0, std::ios::end);
	auto sz = f.tellg();
	f.seekg(0, std::ios::beg);
	out.resize(sz);
	f.read((char*)out.data(), sz);
	return true;
}

bool IsELF(const std::vector<uint8_t>& in) {
	return (in.size() >= 4 && in[0] == 0x7f && in[1] == 'E' && in[2] == 'L' && in[3] == 'F');
}

int main(int argc, char* argv[]) {
	std::cout << "Amiga 500 emulator\n";
	if (argc != 2) {
		std::cerr << "Usage: " << argv[0] << " code.bin\n";
		return 64;
	}


	std::vector<uint8_t> file;
	if (!ReadFile(argv[1], file)) {
		std::cerr << "Cannot read file " << argv[1] << '\n';
		return 66;
	}

	if (IsELF(file)) {
		content
	}

	Context ctx;
	ctx.cpu = std::make_unique<CPU>(&ctx);

	return 0;
}