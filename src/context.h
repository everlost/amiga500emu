#pragma once
#include <memory>

class CPU;
struct Context {
	std::unique_ptr<CPU> cpu;
};