#pragma once
#include <stdint.h>
#include "context.h"
#include "data_stream.h"

struct CPUInternalState_t {
	uint32_t D0, D1, D2, D3, D4, D5, D6, D7;
	uint32_t A0, A1, A2, A3, A4, A5, A6;
	uint32_t A7; // USP & SSP
	uint32_t PC;
	uint16_t SR;
};

class CPU {
private:
	CPUInternalState_t state_;
	Context* ctx_;
public:
	CPU(Context* ctx);
	~CPU() = default;

	bool dispatchAddressingMode();
	bool dispatchOp(DataStream<uint8_t>& code);
};